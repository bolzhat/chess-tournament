Веб-приложение для проведения шахматных турниров по швейцарской системе.
https://bitbucket.org/bolzhat/chess-tournament
http://ru.wikipedia.org/wiki/Швейцарская_система
http://ru.wikipedia.org/wiki/Рейтинг_Эло

Установка:
git clone git@bitbucket.org:bolzhat/chess-tournament.git
cd chess-tournament/
virtualenv .env
source .env/bin/activate
pip install -r requirements.txt

Создание БД и админа:
./manage.py syncdb
По умолчанию используется SQLite 3

Дебаг:
./manage.py runserver
Работает на http://127.0.0.1:8000/
