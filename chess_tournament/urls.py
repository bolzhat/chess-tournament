# -*- coding: utf-8 -*-

from django.conf.urls import patterns, include, url
from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('chess_tournament.views',
    url(r'^$', 'home_view', name='home'),
    url(r'^new/$', 'new_tournament_view', name='new_tournament'),
    url(r'^add_player/$', 'add_player', name='add_player'),
    url(r'^create_tournament/$', 'create_tournament', name='create_tournament'),
    url(r'^(?P<tournament_pk>\d+)/$', 'tournament_view', name='tournament'),
    url(r'^set_game_result/(?P<game_pk>\d+)/$', 'set_game_result', name='set_game_result'),

    url(r'^login/$', 'login_view', name='login'),
    url(r'^logout/$', 'logout_view', name='logout'),

    url(r'^admin/', include(admin.site.urls)),
)
