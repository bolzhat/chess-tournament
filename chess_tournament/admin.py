# -*- coding: utf-8 -*-

from django.contrib import admin
from chess_tournament.models import Player, Tournament, Round, PlayerScore, Game


class PlayerAdmin(admin.ModelAdmin):
    list_display = ['name', 'rating']


class TournamentAdmin(admin.ModelAdmin):
    list_display = ['date']


class RoundAdmin(admin.ModelAdmin):
    list_display = ['tournament', 'number']


class PlayerScoreAdmin(admin.ModelAdmin):
    list_display = ['tournament', 'player', 'score']


class GameAdmin(admin.ModelAdmin):
    list_display = ['tournament', 'round', 'white', 'black', 'result']

admin.site.register(Player, PlayerAdmin)
admin.site.register(Tournament, TournamentAdmin)
admin.site.register(Round, RoundAdmin)
admin.site.register(PlayerScore, PlayerScoreAdmin)
admin.site.register(Game, GameAdmin)
