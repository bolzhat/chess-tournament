$(document).ready(function() {
    $("select.result_selector").change(function() {
        var action = "/set_game_result/" + $(this).attr("game_pk") + "/";
        $("#set_game_result_form").attr("action", action);
        $("#set_game_result_select").append($(this).find("option:selected"));
        $("#set_game_result_form").submit();
    });
});
