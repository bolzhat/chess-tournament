$(document).ready(function() {
    function count_players() {
        if ($("#tournament_players tr").length > 1) {
            $("#create_tournament_button").removeClass("disabled");
        } else {
            $("#create_tournament_button").addClass("disabled");
        }
    }
    
    $("#all_players").on("click", "tr", function(event) {
        $("#tournament_players").append(this);
        count_players();
    });
    
    $("#tournament_players").on("click", "tr", function(event) {
        $("#all_players").append(this);
        count_players();
    });
    
    $("#create_tournament_button").click(function(event) {
        event.preventDefault();
        if (!$(this).hasClass("disabled")) {
            $("#tournament_players tr").each(function(i, row) {
                $("#create_tournament_select").append('<option selected="selected" value="' + $(row).attr("player_pk") + '"></option>');
            });
            $("#create_tournament_form").submit();
        }
    });
    
    $("#new_player_form").submit(function(event) {
        event.preventDefault();
        var valid = true;
        
        $("#new_player_form input.visible").each(function() {
            if ($(this).val()) {
                $(this).parent().removeClass("has-error");
            } else {
                $(this).parent().addClass("has-error");
                valid = false;
            }
        });
        
        if (valid) {
            $.post(
                "/add_player/",
                $(this).serialize(),
                function(data) {
                    $("#tournament_players").append(
                        '<tr player_pk="' + data.player_pk  + '"><td>' + data.rating + '</td><td>' + data.name + '</td>' + 
                        '<td><span class="glyphicon glyphicon-resize-horizontal"></span></td></tr>'
                    );
                    $("#new_player_form input.visible").each(function() {
                        $(this).val("");
                    });
                    count_players();
                },
                "json"
            );
        }
    });
});
