# -*- coding: utf-8 -*-

import json

from django.contrib import messages
from django.http import HttpResponse, HttpResponseBadRequest
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect
from django.utils.translation import ugettext_lazy as _

from chess_tournament.models import Tournament, Player, Game


def home_view(request):
    """Главная страница сайта со списком турниров"""

    tournaments = Tournament.objects.prefetch_related('scores__player', 'rounds')

    for t in tournaments:
        t.top_scores = t.scores.all()[:3]
        t.current_round = t.rounds.all().first().number

    return render(
        request,
        'chess_tournament/home.html',
        {'tournaments': tournaments},
    )


@login_required
def new_tournament_view(request):
    """Страница выбора игроков для нового турнира"""

    return render(
        request,
        'chess_tournament/new.html',
        {'players': Player.objects.all()},
    )


@login_required
def add_player(request):
    """Создает игрока"""
    if request.method == 'POST':
        rating = request.POST.get('rating', '')
        name = request.POST.get('name', '')

        if rating.isdigit() and name:
            player = Player.objects.create(
                rating=int(rating),
                name=name,
            )
            return HttpResponse(json.dumps({
                'player_pk': player.pk,
                'rating': rating,
                'name': name,
            }))

    return HttpResponseBadRequest()


@login_required
def create_tournament(request):
    """Создание турнира"""

    if request.method == 'POST':
        players = request.POST.getlist('players')
        players = [int(player) for player in players if player.isdigit()]

        tournament = Tournament.start(players)

        if tournament is not None:
            return redirect(tournament.get_absolute_url())
        else:
            messages.add_message(request, messages.ERROR, _(u'Не удалось создать турнир'))

    return redirect('new_tournament')


def tournament_view(request, tournament_pk):
    """Страница турнира"""

    t = get_object_or_404(Tournament, pk=tournament_pk)
    t.all_scores = t.scores.all().select_related('player')
    t.all_rounds = t.rounds.all().prefetch_related('games')

    return render(
        request,
        'chess_tournament/tournament.html',
        {'tournament': t},
    )


@login_required
def set_game_result(request, game_pk):
    """Ввод результата игры"""

    game = get_object_or_404(Game, pk=game_pk)
    result = request.POST.get('result', '')

    if request.method != 'POST' or not game.set_result(result):
        messages.add_message(request, messages.ERROR, _(u'Не удалось сохранить результат игры'))

    return redirect(game.round.tournament.get_absolute_url())


@csrf_protect
@never_cache
def login_view(request):
    if request.method == 'POST':
        username = request.POST.get('username', '')
        password = request.POST.get('password', '')

        user = authenticate(username=username, password=password)

        if user and user.is_active:
            login(request, user)
        else:
            messages.add_message(request, messages.ERROR, _(u'Неправильный логин или пароль'))

    return redirect(request.META.get('HTTP_REFERER', 'home'))


def logout_view(request):
    logout(request)
    return redirect(request.META.get('HTTP_REFERER', 'home'))
