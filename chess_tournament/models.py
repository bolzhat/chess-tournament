# -*- coding: utf-8 -*-

from django.db import models
from django.db.models import Q
from django.conf import settings
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _
from django.templatetags.l10n import localize


class Player(models.Model):
    name = models.CharField(_(u'Имя'), max_length=255)
    rating = models.PositiveIntegerField(_(u'Рейтинг'), db_index=True)

    class Meta:
        ordering = ['-rating']
        verbose_name = _(u'Игрок')
        verbose_name_plural = _(u'Игроки')

    def __unicode__(self):
        return self.name

    def add_rating(self, add):
        self.rating += add
        if self.rating < 0:
            self.rating = 0
        self.save()


class Tournament(models.Model):
    date = models.DateField(_(u'Дата начала турнира'), auto_now_add=True)
    players = models.ManyToManyField(Player, verbose_name=_(u'Участники'), through='PlayerScore')
    ended = models.BooleanField(_(u'Закончен'), default=False)

    class Meta:
        ordering = ['-pk']
        verbose_name = _(u'Турнир')
        verbose_name_plural = _(u'Турниры')

    def __unicode__(self):
        return localize(self.date)

    def get_absolute_url(self):
        return reverse('tournament', args=(self.pk,))

    @classmethod
    def start(cls, player_ids):
        players = Player.objects.filter(pk__in=player_ids)

        if players.count() < 2:
            return None

        tournament = cls.objects.create()

        for player in players:
            PlayerScore.objects.create(player=player, tournament=tournament)

        tournament.new_round()

        return tournament

    def new_round(self):
        n_rounds = self.rounds.all().count()
        if n_rounds < settings.MAX_ROUNDS_NUMBER:
            Round.objects.create(
                number=n_rounds + 1,
                tournament=self,
            ).check_games()
        else:
            self.end()

    def end(self):
        self.ended = True
        self.save()


class PlayerScore(models.Model):
    player = models.ForeignKey(Player)
    tournament = models.ForeignKey(Tournament, related_name='scores')
    score = models.FloatField(_(u'Очки'), default=0, db_index=True)

    class Meta:
        ordering = ['-score']
        verbose_name = _(u'Очки')
        verbose_name_plural = _(u'Очки')

    def __unicode__(self):
        return u'{} {}'.format(self.player, self.score)


class Round(models.Model):
    number = models.PositiveIntegerField(_(u'Номер тура'), db_index=True)
    tournament = models.ForeignKey(Tournament, verbose_name=_(u'Турнир'), related_name='rounds')

    class Meta:
        ordering = ['-number']
        verbose_name = _(u'Тур')
        verbose_name_plural = _(u'Туры')

    def __unicode__(self):
        return unicode(self.number)

    def check_games(self):
        if not self.games.exists():
            # Создать пары игроков
            scores = self.tournament.scores.all().select_related('player')
            players = [score.player for score in scores]
            found_pair = []
            without_pair = []

            for i, a in enumerate(players):
                if a in found_pair:
                    continue

                for b in players[i + 1:]:
                    if b in found_pair:
                        continue

                    if not Game.objects.filter(
                        Q(white=a, black=b) |
                        Q(black=a, white=b),
                        tournament=self.tournament,
                    ).exists():
                        # За белых играет тот, кто провел ими меньше игр в турнире
                        a_white = Game.objects.filter(
                            tournament=self.tournament,
                            white=a,
                        ).count()
                        b_white = Game.objects.filter(
                            tournament=self.tournament,
                            white=b,
                        ).count()

                        if a_white > b_white:
                            white, black = b, a
                        else:
                            white, black = a, b

                        Game.objects.create(
                            tournament=self.tournament,
                            round=self,
                            white=white,
                            black=black,
                        )
                        found_pair.append(a)
                        found_pair.append(b)
                        break
                else:
                    without_pair.append(a)

            if len(without_pair) == 1:
                # Последнему дается очко
                s = PlayerScore.objects.get(
                    player=without_pair[0],
                    tournament=self.tournament,
                )
                s.score += 1
                s.save()
            elif len(without_pair) > 1:
                # Конец турнира
                self.games.all().delete()
                self.delete()
                self.tournament.end()

        elif not self.games.filter(result='pending').exists():
            # Все игры текущего раунда закончены
            self.tournament.new_round()


class Game(models.Model):
    GAME_RESULT = (
        ('pending', _(u'Ожидается')),
        ('white', _(u'Победа белых')),
        ('black', _(u'Победа черных')),
        ('draw', _(u'Ничья')),
    )

    ADD_POINTS = {
        'white': (1, 0),
        'black': (0, 1),
        'draw': (0.5, 0.5),
    }

    tournament = models.ForeignKey(Tournament, verbose_name=_(u'Турнир'), related_name='games')
    round = models.ForeignKey(Round, verbose_name=_(u'Тур'), related_name='games')
    white = models.ForeignKey(Player, verbose_name=_(u'Игрок белыми'), related_name='white_games')
    black = models.ForeignKey(Player, verbose_name=_(u'Игрок черными'), related_name='black_games')
    result = models.CharField(_(u'Результат'), choices=GAME_RESULT, default='pending', max_length=7)

    class Meta:
        ordering = ['pk']
        verbose_name = _(u'Игра')
        verbose_name_plural = _(u'Игры')

    def __unicode__(self):
        return u'{} : {} - {}'.format(self.white, self.black, self.result)

    def set_result(self, result):
        """Указать результат игры"""

        if result in [
            'white',
            'black',
            'draw',
        ] and self.result == 'pending':
            # Сохранить результат игры
            self.result = result
            self.save()

            # Изменить очки игроков
            white_points, black_points = self.ADD_POINTS[result]
            self.add_points(self.white, white_points)
            self.add_points(self.black, black_points)

            # Пересчитать рейтинг игроков
            white_rating = self.compute_rating(self.white, self.black, white_points)
            black_rating = self.compute_rating(self.black, self.white, black_points)
            self.white.add_rating(white_rating)
            self.black.add_rating(black_rating)

            self.round.check_games()
            return True

        return False

    def add_points(self, player, points):
        if points:
            score = PlayerScore.objects.get(
                tournament=self.round.tournament,
                player=player,
            )
            score.score += points
            score.save()

    def compute_rating(self, player, opponent, points):
        """Изменение рейтинга Эло"""
        E = 1.0 / (1 + 10 ** (float(opponent.rating - player.rating) / 400))
        K = 10 if player.rating >= 2400 else 15
        return K * (points - E)
